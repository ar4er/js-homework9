// Опишіть, як можна створити новий HTML тег на сторінці.
// document.body.innerHTML = `<p></p>`
// document.body.append(document.createElement("div"));
// document.body.insertAdjacentHTML("afterbegin", "<div>ololo</div>")

// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Место встаки тега в родительский. beforebegin - перед открывающим родительским тегом, afterbegin - сразу после открывающего родительского тега,
// beforeend - перед закрывающим тегом родительского елемента, afterend - сразу после закрывающего тега родительского елемента.

// Як можна видалити елемент зі сторінки?
// element.innerHTML = ""

const array1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

const array2 = ["1", "2", "3", "sea", "user", 23];

const array3 = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
  [
    "Odessa",
    ["Odessa", "Lviv", "Dnieper", ["Odessa", "Lviv", "Dnieper"]],
    "Lviv",
    "Dnieper",
    [
      "Odessa",
      "Lviv",
      "Dnieper",
      ["Odessa", "Lviv", "Dnieper", ["Odessa", "Lviv", "Dnieper"]],
    ],
  ],
];

const timer = document.createElement("div");
timer.style.position = "absolute";
timer.style.fontSize = "100px";
timer.style.top = "50%";
timer.style.left = "50%";
timer.style.transform = "translate(-50%, -50%)";
timer.style.textAlign = "center";
timer.innerText = "Нажмите на текст для запуска таймера.";
document.body.append(timer);

// Через цикл for...of
// function arrayToList(arr, parent = document.body) {
//   const list = document.createElement("ul");
//   parent.append(list);
//   for (let element of arr) {
//     if (Array.isArray(element)) {
//       arrayToList(element, list);
//     } else {
//       const listItem = document.createElement("li");
//       list.append(listItem);
//       listItem.innerText = element;
//     }
//   }
// }

// Через цикл forEach
function arrayToList(arr, parent = document.body) {
  const list = document.createElement("ul");
  parent.append(list);
  arr.forEach((element) => {
    if (Array.isArray(element)) {
      arrayToList(element, list);
    } else {
      const listItem = document.createElement("li");
      list.append(listItem);
      listItem.innerText = element;
    }
  });
}

arrayToList(array1);
arrayToList(array2);
arrayToList(array3);

function clearBody() {
  document.body.innerHTML = "";
}

document.body.addEventListener("click", () => {
  timer.style.fontSize = "500px";
  timer.innerText = 3;
  let timerStart = setInterval(() => timer.innerText--, 1000);
  setTimeout(() => {
    clearInterval(timerStart);
    clearBody();
  }, 3000);
});
